% Run Monte-Carlo simulation of orfice uncertainty
numIt = 5e3;    % Number of iterations

% Orifice parameters; leave these unchanged
Do = 0.05; % m
Di = 0.03; % m
p1_mean = 125e3;        % Pa
p2_mean = 110e3;        % Pa
T1_mean = 90+273.15;    % K
% Uncertainties
up1 = 100;
up2 = 100;
uT1 = 0.3;
% end Orifice parameters

tic
for k = 1 : numIt
p1r(k) = normrnd(p1_mean,up1);
p2r(k) = normrnd(p2_mean,up2);
T1r(k) = normrnd(T1_mean,uT1);
mu1(k) = 145.8*T1r(k)^1.5/(T1r(k)+110.4)*1e-8; % Sutherland's law
MF(k) = Massflow(p1r(k),p2r(k),T1r(k),Di,Do,mu1(k));
end
toc

histogram (MF);
mean (MF)
std (MF)

